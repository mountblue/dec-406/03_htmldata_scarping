'''
    Name:           htmlTableScript.py
    Description:    A python script to fetch the html table data and show a tabular view in terminal
    input:          .html file containing the table
    output:         tabular view with column name and row value
    usage:          python htmlTableScript.py
                            <filename:>

    example:        python htmlTableScript.py
                            htmlTableScript.csv
'''
#importing PrettyTable library to show table
from prettytable import PrettyTable

#method to open and read a file
def fileOpen(filename):
        filedata = open(filename,'r')
        return filedata

#method to fetch table headers and row data
def html_data(filename):
    table_headers = []
    resJson = []
    rowVal = {}
    count = 0

    filedata = fileOpen(filename)
    data = filedata.readline()

    while '</tr>' not in data:
        if '<th>' in data:
            index1 = data.index('<th>')+4
            index2 = data.index('</th>')
            table_headers.append(data[index1 : index2])

        data = filedata.readline()

    data = filedata.readline()

    while '</table>' not in data:
        if '<td>' in data:
            indexSrart = data.index('<td>') + 4
            indexEnd = data.index('</td>')
            rowVal[table_headers[count]] = data[indexSrart:indexEnd]
            count += 1

        if '</tr>' in data:
            count = 0
            resJson.append(rowVal)
            rowVal = {}
        data = filedata.readline()

    return resJson, table_headers

#method to generate output table
def gen_table(filename):
    table_data, table_headers = html_data(filename)
    p_table = PrettyTable()

    p_table.field_names = table_headers

    for data in table_data:
        row_data = []
        for header in table_headers:
            row_data.append(data[header])
        p_table.add_row(row_data)

    return p_table

#function to get file name and show the table
def main():
    while True:

        filename = input('Enter the filename: ',)
        filesplit = filename.split('.')
        try:
            if filesplit[1] == 'html':
                print("Filename and filetype is valid")
                break
            else:
                print("Filetype is not valid")
        except IndexError:
            print("Enter a valid html file")

    html_data_table = gen_table(filename)
    print(html_data_table)

#calling main function
if __name__ == '__main__':
    main()
